drop trigger if exists one_to_many_employee_has_post;
delimiter //
create trigger one_to_many_employee_has_post
before delete
on post for each row
begin
declare count_post_usage int;
set count_post_usage = (select count(e.id) from employee e where e.post = old.post);
if count_post_usage > 0 then
	signal sqlstate '45000' set message_text = 'error one_to_many_employee_has_post';
end if;
end;//
delimiter ;

drop trigger if exists one_to_many_employee_has_post_before_update_post;
delimiter //
create trigger one_to_many_employee_has_post_before_update_post
before update
on post for each row
begin
declare val varchar(15);
select post into val from employee e where e.post = old.post;
if val is not null then
	signal sqlstate '45000' set message_text = 'error one_to_many_employee_has_post_before_update_post';
end if;
end;//
delimiter ;


  -- insert trigger
drop trigger if exists one_to_many_employee_has_post_before_insert;
delimiter //
create trigger one_to_many_employee_has_post_before_insert
before insert
on employee for each row
begin
declare val varchar(15);
select post into val from post p where p.post = new.post;
if val is null then
	signal sqlstate '45000' set message_text = 'here no such post';
end if;
end;//
delimiter ;
-- update trigger
drop trigger if exists one_to_many_employee_has_post_before_update;
delimiter //
create trigger one_to_many_employee_has_post_before_update
before update
on employee for each row
begin
declare val varchar(15);
select post into val from post p where p.post = new.post;
if val is null then
	signal sqlstate '45000' set message_text = 'here no such post';
end if;
end;//
delimiter ;

-- 1:m employee pharmacy

drop trigger if exists one_to_many_employee_has_pharmacy;
delimiter //
create trigger one_to_many_employee_has_pharmacy
before delete
on pharmacy for each row
begin
declare count_pharmacy_usage int;
set count_pharmacy_usage = (select count(e.id) from employee e where e.pharmacy_id = old.id);
if count_pharmacy_usage > 0 then
	signal sqlstate '45000' set message_text = 'error one_to_many_employee_has_pharmacy';
end if;
end;//
delimiter ;

drop trigger if exists one_to_many_employee_has_pharmacy_before_update_pharmacy;
delimiter //
create trigger one_to_many_employee_has_pharmacy_before_update_pharmacy
before update
on pharmacy for each row
begin
declare count_pharmacy_usage int;
set count_pharmacy_usage = (select count(e.id) from employee e where e.pharmacy_id = old.id);
if count_pharmacy_usage > 0 then
	signal sqlstate '45000' set message_text = 'error one_to_many_employee_has_pharmacy_before_update_pharmacy';
end if;
end;//
delimiter ;


drop trigger if exists one_to_many_employee_has_pharmacy_before_insert;
delimiter //
create trigger one_to_many_employee_has_pharmacy_before_insert
before insert
on employee for each row
begin
if new.pharmacy_id is not null then
begin
	declare val int;
	select id into val from pharmacy p where p.id = new.pharmacy_id;
	if val is null then
		signal sqlstate '45000' set message_text = 'error one_to_many_employee_has_pharmacy_before_insert';
	end if;
end;
end if;
end;//
delimiter ;


drop trigger if exists one_to_many_employee_has_pharmacy_before_update;
delimiter //
create trigger one_to_many_employee_has_pharmacy_before_update
before update
on employee for each row
begin
declare val int;
select id into val from pharmacy p where p.id = new.pharmacy_id;
if val is null then
	signal sqlstate '45000' set message_text = 'error one_to_many_employee_has_pharmacy_before_update';
end if;
end;//
delimiter ;

-- 1:m street pharmacy

drop trigger if exists one_to_many_pharmacy_has_street;
delimiter //
create trigger one_to_many_pharmacy_has_street
before delete
on street for each row
begin
declare count_street_usage int;
set count_street_usage = (select count(p.id) from pharmacy p where p.street = old.street);
if count_street_usage > 0 then
	signal sqlstate '45000' set message_text = 'post record is used in employee one_to_many_pharmacy_has_street';
end if;
end;//
delimiter ;

drop trigger if exists one_to_many_pharmacy_has_street_before_update_street;
delimiter //
create trigger one_to_many_pharmacy_has_street_before_update_street
before update
on street for each row
begin
declare count_street_usage int;
set count_street_usage = (select count(p.id) from pharmacy p where p.street = old.street);
if count_street_usage > 0 then
	signal sqlstate '45000' set message_text = 'post record is used in employee';
end if;
end;//
delimiter ;

drop trigger if exists one_to_many_pharmacy_has_street_before_insert;
delimiter //
create trigger one_to_many_pharmacy_has_street_before_insert
before insert
on pharmacy for each row
begin
declare val varchar(25);
select street into val from street s where s.street = new.street;
if val is null then
	signal sqlstate '45000' set message_text = 'error one_to_many_pharmacy_has_street_before_insert';
end if;
end;//
delimiter ;

drop trigger if exists one_to_many_pharmacy_has_street_before_update;
delimiter //
create trigger one_to_many_pharmacy_has_street_before_update
before update
on pharmacy for each row
begin
declare val varchar(25);
select street into val from street s where s.street = new.street;
if val is null then
	signal sqlstate '45000' set message_text = 'error one_to_many_pharmacy_has_street_before_update';
end if;
end;//
delimiter ;

-- m:m pharmacy medicine

drop trigger if exists many_to_many_pharmacy_medicine_delete_pharmacy;
delimiter //
create trigger many_to_many_pharmacy_medicine_delete_pharmacy
before delete
on pharmacy for each row
begin
declare val int;
select count(id) into val from pharmacy_medicine pm where pm.pharmacy_id = old.id;
if val > 0 then
	signal sqlstate '45000' set message_text = 'error many_to_many_pharmacy_medicine_delete_pharmacy';
end if;
end;//
delimiter ;

drop trigger if exists many_to_many_pharmacy_medicine_delete_medicine;
delimiter //
create trigger many_to_many_pharmacy_medicine_delete_medicine
before delete
on medicine for each row
begin
declare val int;
select count(id) into val from pharmacy_medicine pm where pm.medicine_id = old.id;
if val > 0 then
	signal sqlstate '45000' set message_text = 'error many_to_many_pharmacy_medicine_delete_medicine';
end if;
end;//
delimiter ;

drop trigger if exists many_to_many_pharmacy_medicine_update_medicine;
delimiter //
create trigger many_to_many_pharmacy_medicine_update_medicine
before update
on medicine for each row
begin
if old.id != new.id then
	signal sqlstate '45000' set message_text = 'error many_to_many_pharmacy_medicine_update_medicine';
end if;
end;//
delimiter ;

drop trigger if exists many_to_many_pharmacy_medicine_update_pharmacy;
delimiter //
create trigger many_to_many_pharmacy_medicine_update_pharmacy
before update
on pharmacy for each row
begin
if old.id != new.id then
	signal sqlstate '45000' set message_text = 'error many_to_many_pharmacy_medicine_update_pharmacy';
end if;
end;//
delimiter ;

drop trigger if exists many_to_many_pharmacy_medicine_before_insert;
delimiter //
create trigger many_to_many_pharmacy_medicine_before_insert
before insert
on pharmacy_medicine for each row
begin
declare medicine_id int;
declare pharmacy_id int;
select id into medicine_id from medicine m where m.id = new.medicine_id;
select id into pharmacy_id from pharmacy p where p.id = new.pharmacy_id;
if medicine_id is null or pharmacy_id is null then
	signal sqlstate '45000' set message_text = 'error many_to_many_pharmacy_medicine_before_insert';
end if;
end;//
delimiter ;

-- m:m medicine zone

drop trigger if exists many_to_many_medicine_zone_delete_medicine;
delimiter //
create trigger many_to_many_medicine_zone_delete_medicine
before delete
on medicine for each row
begin
declare val int;
select count(id) into val from medicine_zone mz where mz.zone_id = old.id;
if val > 0 then
	signal sqlstate '45000' set message_text = 'error many_to_many_medicine_zone_delete_medicine';
end if;
end;//
delimiter ;

drop trigger if exists many_to_many_medicine_zone_delete_zone;
delimiter //
create trigger many_to_many_medicine_zone_delete_zone
before delete
on zone for each row
begin
declare val int;
select count(id) into val from medicine_zone mz where mz.zone_id = old.id;
if val > 0 then
	signal sqlstate '45000' set message_text = 'error many_to_many_medicine_zone_delete_zone';
end if;
end;//
delimiter ;

drop trigger if exists many_to_many_medicine_zone_update_medicine;
delimiter //
create trigger many_to_many_medicine_zone_update_medicine
before update
on medicine for each row
begin
if old.id != new.id then
	signal sqlstate '45000' set message_text = 'error many_to_many_medicine_zone_update_medicine';
end if;
end;//
delimiter ;

drop trigger if exists many_to_many_medicine_zone_update_zone;
delimiter //
create trigger many_to_many_medicine_zone_update_zone
before update
on zone for each row
begin
if old.id != new.id then
	signal sqlstate '45000' set message_text = 'error many_to_many_medicine_zone_update_zone';
end if;
end;//
delimiter ;

drop trigger if exists many_to_many_medicine_zone_before_insert;
delimiter //
create trigger many_to_many_medicine_zone_before_insert
before insert
on medicine_zone for each row
begin
declare medicine_id int;
declare zone_id int;
select id into medicine_id from medicine m where m.id = new.medicine_id;
select id into zone_id from zone z where z.id = new.zone_id;
if medicine_id is null or zone_id is null then
	signal sqlstate '45000' set message_text = 'error many_to_many_medicine_zone_before_insert';
end if;
end;//
delimiter ;


-- triggers task
-- identity

drop trigger if exists employee_before_insert;
delimiter //
create trigger employee_before_insert
before insert
on employee for each row
begin
if new.identity_number like '%00' then
	signal sqlstate '45000' set message_text = 'error employee_before_insert';
end if;
end;//
delimiter ;


drop trigger if exists employee_before_update;
delimiter //
create trigger employee_before_update
before update
on employee for each row
begin
if new.identity_number like '%00' then
	signal sqlstate '45000' set message_text = 'error employee_before_update';
end if;
end;//
delimiter ;

-- trigger ministry_code
drop trigger if exists medicine_before_insert;
delimiter //
create trigger medicine_before_insert
before insert
on medicine for each row
begin
if new.ministry_code not rlike '[^MP]{2}-\\d{3}-\\d{2}' then
	signal sqlstate '45000' set message_text = 'error medicine_before_insert';
end if;
end;//
delimiter ;

-- forbid modification in post trigger

drop trigger if exists post_before_update;
delimiter //
create trigger post_before_update
before update
on post for each row
begin
signal sqlstate '45000' set message_text = 'error post_before_update';
end;


-- stored procedures

delimiter //
create procedure insert_employee(psurname varchar(30), pname char(30), pidentity_number char(10), ppost varchar(15))
begin
	insert into employee(surname, name, identity_number, post, pharmacy_id)
    values(psurname, pname, pidentity_number, ppost);
end//
delimiter ;


delimiter //
create procedure insert_medicine_zone(medicine_name varchar(30), zone_name varchar(25))
begin
	declare m_id int;
    declare z_id int;
	select id into m_id from medicine m where m.name = medicine_name;
    select id into z_id from zone z where z.name = zone_name;
	insert into medicine_zone(medicine_id, zone_id)
    values(m_id, z_id);
end//
delimiter ;

drop procedure if exists my_cursor;
delimiter //
create procedure my_cursor()
begin
	declare done int;
    declare x int;
    declare surname_tmp varchar(30);
	declare random int;
    declare mycursor cursor for select surname from employee;
    declare continue handler for not found set done = true;
    set done = false;

    open mycursor;
    my_loop: loop
    fetch mycursor into surname_tmp;
    set x = 0;
    set random = FLOOR(RAND()*(9)+1);
    if done = true then leave my_loop;
    end if;
    set @tmp_query = concat('create table ', surname_tmp,'(');
    repeat
		set @tmp_query = concat (@tmp_query, 'row', x, ' varchar(10)');
		set x = x + 1;
        if x < random then
			set @tmp_query = concat(@tmp_query, ',');
		end if;
    until x >= random
    end repeat;
    set @tmp_query = concat(@tmp_query, ');');
    prepare myquery from @tmp_query;
    execute myquery;
    deallocate prepare myquery;
    end loop;
    close mycursor;
end//
delimiter ;

-- functions

delimiter //
create function min_exp() returns int
deterministic
begin
	return (select min(experience) from employee);
end //
delimiter ;

select experience from employee e where e.experience = min_exp();

delimiter //
create function get_pharmacy_address(pharmacy_id int) returns int
deterministic
begin
	return (select concat(name, ' ', building_number) from pharmacy where id = pharmacy_id);
end //
delimiter ;

select get_pharmacy_address(pharmacy_id) from employee;
